#include "Menue.h"
#include <array>

// By Jessica

void Menue::Initialize(Renderer* _ptr_renderer)
{
#if _DEBUG
	m_ptr_colliderTexture = new Texture("Assets\\Debug\\ColliderTexture.png", _ptr_renderer);
	m_ptr_triggerTexture = new Texture("Assets\\Debug\\TriggerTexture.png", _ptr_renderer);
#endif

	m_ptr_Renderer = _ptr_renderer;
	m_isRunning = true;

	// MAIN MENUE
	m_ptr_mainMenue = new Image("Assets\\MainMenue.png", Rect(Vector2(0, 0), Vector2(1920, 1080)), this, 0);

	// START GAME
	m_ptr_startGame = new Trigger(Rect(Vector2(130, 450), Vector2(575, 200)), this, 1);

}

void Menue::Update(float _deltaTime) 
{
	if (Input::MouseDown(0)
		&& !(Input::GetMousePosition().m_X >= m_ptr_startGame->m_Position.m_X + m_ptr_startGame->m_Resolution.m_X
			|| Input::GetMousePosition().m_X <= m_ptr_startGame->m_Position.m_X
			|| Input::GetMousePosition().m_Y >= m_ptr_startGame->m_Position.m_Y + m_ptr_startGame->m_Resolution.m_Y
			|| Input::GetMousePosition().m_Y <= m_ptr_startGame->m_Position.m_Y))
	{
		m_isRunning = false;
	}

	for (int i = 0; i < m_EntityList.size(); i++)
	{
		if (m_EntityList[i] != nullptr)
		{
			m_EntityList[i]->Update(_deltaTime);
		}
	}
}
void Menue::Render(Renderer* _ptr_renderer)
{
	for (int i = 0; i < m_EntityList.size(); i++)
	{
		if (m_EntityList[i] != nullptr)
		{
			m_EntityList[i]->Render(_ptr_renderer);
		}
	}

#if _DEBUG
	for each (Collider* collider in m_ColliderList)
	{
		if (collider->m_IsTrigger)
		{
			_ptr_renderer->DrawTexture(m_ptr_triggerTexture, Rect(collider->m_Position, collider->m_Resolution));
		}
		else
		{
			_ptr_renderer->DrawTexture(m_ptr_colliderTexture, Rect(collider->m_Position, collider->m_Resolution));
		}
	}
#endif
}
