#include "Game.h"
#include "Entity.h"

void Game::Initialize(Renderer* _ptr_renderer)
{
	// The following has to be included
	m_ptr_Renderer = _ptr_renderer;
	m_isRunning = true;
}
void Game::Update(float _deltaTime)
{
	// The following has to be included
	for each(Entity* entity in m_EntityList)
	{
		entity->Update(_deltaTime);
	}
}
void Game::Render(Renderer* _ptr_renderer)
{
	// the following has to be included
	for each(Entity* entity in m_EntityList)
	{
		entity->Render(_ptr_renderer);
	}
}