#pragma once
#include "Texture.h"
#include "Entity.h"
#include "Game.h"
#include "Renderer.h"

// By Viki

class Image : public Entity
{
private:
	Texture* m_ptr_Texture;

public:
	Image(char* _ptr_Filename, Rect _posRect, Game* _ptr_Game, int _layer);
	~Image();
	void Render(Renderer* _ptr_renderer) override;

};