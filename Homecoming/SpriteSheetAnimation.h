#pragma once
#include "Renderer.h"
#include "AnimationClip.h"

class SpriteSheetAnimation
{
private:
	Texture* m_ptr_texture;
	int m_currentFrame;
	float m_spf;
	float m_deltaSpf;
	Vector2 m_frameResolution;
	Renderer* m_ptr_renderer;

public:
	AnimationClip* m_ptr_CurrentAnimation;

	///<summary> Imports a spritesheet
	///<para> _spf is the time each frame will be shown in seconds
	///</para>
	///<para> _resolution is the resolution of each single texture
	///</para>
	///</summary>
	SpriteSheetAnimation(char* _filename, float _spf, Vector2 _frameResolution, Renderer* p_pRenderer);
	~SpriteSheetAnimation();
	
	///<summary> Called once per frame when placed in entity update
	///<para> Clip needs to be in a single line
	///</para>
	///</summary>
	void AUpdate(float _deltaTime);
	void ADraw(Vector2 _position, Vector2 _resolution);
};