#pragma once
#include "Vector2.h"
#include "Entity.h"
#include "Rect.h"

class Physics;

class Collider
{
private:
	Entity* m_ptr_entity;
	Physics* m_ptr_physics;
	Vector2 m_prevPos;
	Vector2 m_offset;
	Vector2 m_overlap;
	bool m_isDynamic;
	bool m_overlappedX;
	bool m_overlappedY;

public:
	Vector2 m_Position;
	Vector2 m_Resolution;
	Collider* m_ptr_ColliderHit;
	bool m_IsGrounded;
	bool m_IsTrigger;

	///<summary> Creates a collider at the position and size of physics parent entity
	///<para> Takes physics of an entity and an additional offset for positioning
	///</para>
	///<para> Needs pointer to physics of specified entity
	///</para>
	///</summary>
	Collider(Vector2 _offset, Vector2 _resolution, bool _isDynamic, bool _isTrigger, Physics* _ptr_physics);
	~Collider();

	Vector2 IntersectRect(Rect _rectA, Rect _rectB);
	///<summary> Returns whether the colider hit another collider
	///<para> Collider hit is passed to m_ColliderHit
	///</para>
	///</summary>
	bool GetCollisionState();
	///<summary> Returns whether the collider hit another specific collider
	///<para> Collider hit is passed to m_ColliderHit
	///</para>
	///</summary>
	bool GetCollisionState(Collider* _ptr_collider);
	void CollisionResponse();
	///<summary> Processes collision once per frame
	///</summary>
	void CUpdate();
};