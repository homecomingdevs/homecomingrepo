#pragma once
#include "SDLInclude.h"
#include "Rect.h"

class Renderer;

class Texture
{
private:
	SDL_Texture* m_ptr_texture;
	
public:
	Texture(char* _ptr_filename, Renderer* _ptr_renderer);
	~Texture();

	SDL_Texture* GetTexture();
};