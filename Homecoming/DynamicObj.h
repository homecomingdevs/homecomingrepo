#pragma once
#include "Entity.h"
#include "Texture.h"
#include "Physics.h"

class DynamicObj : public Entity
{
private:
	Texture* m_ptr_texture;
	Physics* m_ptr_physics;

public:
	int m_MoveSpeed;

	DynamicObj(char* _ptr_filename, Rect _posRes, Game* _ptr_game, int _layer);
	~DynamicObj();

	void Update(float _deltaTime) override;
	void Render(Renderer* _ptr_renderer) override;
};