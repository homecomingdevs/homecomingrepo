#pragma once
#include "SDLInclude.h"
#include "Vector2.h"

class Rect : public SDL_Rect
{
public:
	Rect();
	Rect(Vector2 _position, Vector2 _resolution);
};