#pragma once
#include "Renderer.h"
#include "Image.h"
#include "Game.h"
#include "Trigger.h"
#include "Input.h"

// By Jessica

class Menue : public Game 
{
private:
#if _DEBUG
	Texture* m_ptr_colliderTexture;
	Texture* m_ptr_triggerTexture;
#endif

	// MAIN MENUE
	Image* m_ptr_mainMenue;

	// START GAME
	Trigger* m_ptr_startGame;

	bool menueClosed;

public:

	void Initialize(Renderer* _ptr_renderer) override;
	void Update(float _deltaTime) override;
	void Render(Renderer* _ptr_renderer) override;
};
