#pragma once
#include "SDLInclude.h"

class Vector2;

class Vector2
{
public:
	float m_X;
	float m_Y;

	Vector2();
	Vector2(int _x, int _y);

	Vector2 Add(Vector2 _toAdd);
};