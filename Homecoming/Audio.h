#pragma once
#include "SDLInclude.h"
#include <SDL_mixer.h>

class Audio
{
private:
	Mix_Music* m_music;
	Mix_Chunk* m_chunk;
	bool m_isMusic;

public:
	///<summary> Imports audioclip
	///</summary>
	Audio(char* Filename, bool _isMusic);
	~Audio();

	///<summary> Play imported audioclip
	///</summary>
	void Play();
	///<summary> Pause audioclip
	///</summary>
	void Pause();
	///<summary> Resume audioclip
	///</summary>
	void Resume();
	///<summary> halt audioclip
	///</summary>
	void Stop();
};