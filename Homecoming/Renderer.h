#pragma once
#include "SDLInclude.h"
#include "Rect.h"
#include "Font.h"

class Texture;

class Renderer
{
private:
	SDL_Renderer* m_ptr_renderer;

public:
	Renderer(SDL_Window* _ptr_window);
	SDL_Renderer* GetRenderer();

	void Clear();
	void Present();
	/// <summary> Renders a texture
	/// <para> _destination sets position and resolution in pixel
	/// </para>
	/// </summary>
	void DrawTexture(Texture* _ptr_texture, Rect _destination);
	/// <summary> Renders a part of a texture
	/// <para> _source sets part of the texture in pixel
	/// </para>
	/// <para> _destination sets position and resolution in pixel
	/// </para>
	/// </summary>
	void DrawTexture(Texture* _ptr_texture, Rect _source, Rect _destination);
	///<summary> Renders Text with overflow
	///</summary>
	void DrawText(char* _ptr_text, Font* _ptr_font, SDL_Color _color, Vector2 _position);
};
