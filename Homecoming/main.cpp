#include <iostream>
#include "HEngine.h"
#include "HomecomingGame.h"
#include "Menue.h"

using namespace std;

int main(int _Argc, char** _Argv)
{
	HEngine* ptr_hEngine = new HEngine();
	ptr_hEngine->Initialize("Homecoming", Rect(Vector2(0, 0), Vector2(1920, 1080)));
	ptr_hEngine->Run(new Menue());
	ptr_hEngine->Run(new HomecomingGame());
	ptr_hEngine->Finalize();
	return 0;
}