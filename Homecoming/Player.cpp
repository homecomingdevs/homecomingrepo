#include "Player.h"
#include "Input.h"

Player::Player(Game* _ptr_game, int _layer) : Entity(_ptr_game, _layer)
{
	m_moveSpeed = 500;
	m_jumpSpeed = -900;
	m_Position = Vector2(500, 100);
	m_Resolution = Vector2(300, 300);
	m_ptr_physics = new Physics(this);
	m_ptr_physics->AddCollider(Vector2(100, 0), Vector2(100,m_Resolution.m_Y - 30), true, false);
	m_ptr_physics->AddGravity(2000.0f);
	m_ptr_anim = new SpriteSheetAnimation("Assets\\Character\\ChanningAnimationSheet.png", 10.0f, Vector2(700,700), _ptr_game->m_ptr_Renderer);
	m_ptr_idleRight = new AnimationClip(0, 0, 1);
	m_ptr_idleLeft = new AnimationClip(1, 0, 1);
	m_ptr_walkRight = new AnimationClip(2, 1, 2);
	m_ptr_walkLeft = new AnimationClip(0, 2, 2);
	m_ptr_idle = m_ptr_idleRight;
	m_ptr_ControlEnabled = true;
	m_ptr_walking = new Audio("Assets\\Sound\\Walking.wav", false); //Fails because .wav-file is needed
	m_ptr_walking->Play();
}

void Player::Update(float _deltaTime)
{
	m_ptr_anim->m_ptr_CurrentAnimation = m_ptr_idle;
	m_ptr_walking->Pause();
	if (m_ptr_ControlEnabled)
	{
		if ((Input::KeyDown(SDL_Scancode::SDL_SCANCODE_W) || Input::KeyDown(SDL_Scancode::SDL_SCANCODE_SPACE) || Input::KeyDown(SDL_Scancode::SDL_SCANCODE_UP))
			&& !(Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_W) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_SPACE) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_UP))
			&& m_ptr_physics->m_ptr_Collider->m_IsGrounded)
		{
			m_Speed.m_Y = m_jumpSpeed;
		}
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_A) || Input::KeyDown(SDL_Scancode::SDL_SCANCODE_LEFT))
		{
			m_Speed.m_X = -m_moveSpeed;
			m_ptr_anim->m_ptr_CurrentAnimation = m_ptr_walkLeft;
			m_ptr_idle = m_ptr_idleLeft;
			m_ptr_walking->Resume();
		}
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_D) || Input::KeyDown(SDL_Scancode::SDL_SCANCODE_RIGHT))
		{
			m_Speed.m_X = m_moveSpeed;
			m_ptr_anim->m_ptr_CurrentAnimation = m_ptr_walkRight;
			m_ptr_idle = m_ptr_idleRight;
			m_ptr_walking->Resume();
		}
	}
	this->m_ptr_physics->PUpdate(_deltaTime);
	m_Speed.m_X = 0;
	this->m_ptr_anim->AUpdate(_deltaTime);
}
void Player::Render(Renderer* _ptr_renderer)
{
	this->m_ptr_anim->ADraw(m_Position, m_Resolution);
}