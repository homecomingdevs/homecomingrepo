#include "Entity.h"
#include "Game.h"
#include "Physics.h"

Entity::Entity(Game* _ptr_game, int _layer)
{
	m_ptr_Game = _ptr_game;
	m_layer = _layer;
	m_ptr_Game->m_EntityList[m_layer] = this;
}
Entity::~Entity()
{
	m_ptr_Game->m_EntityList[m_layer] = nullptr;
}

void Entity::Update(float _deltaTime)
{
	// Physics Update
	//	 this->m_ptr_Physics->PUpdate(_deltaTime);
	// Animation Update
	//	 this->m_ptr_spriteSheetAnimation->AUpdate(this->m_ptr_AnimationClip, this->m_Position, _deltaTime);
}
void Entity::Render(Renderer* _ptr_renderer)
{
	// All Rendermethods needed for this Entity
	//	 example: _ptr_renderer->DrawTexture(m_ptr_texture, Rect(this->m_Position, this->m_Resolution));
}