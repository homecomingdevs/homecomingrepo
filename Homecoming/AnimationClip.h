#pragma once

class AnimationClip
{
public:
	int m_Column;
	int m_Line;
	int m_Frames;

	///<summary> Defines the series of textures for the animation
	///</summary>
	AnimationClip(int _column, int _line, int _frames);
};