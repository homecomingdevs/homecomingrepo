#include "HomecomingGame.h"
#include <array>

void HomecomingGame::Initialize(Renderer* _ptr_renderer)
{
#if _DEBUG
	m_ptr_colliderTexture = new Texture("Assets\\Debug\\ColliderTexture.png", _ptr_renderer);
	m_ptr_triggerTexture = new Texture("Assets\\Debug\\TriggerTexture.png", _ptr_renderer);
#endif

	m_ptr_Renderer = _ptr_renderer;
	m_isRunning = true;

	// ObjectList
	// Draw Level: Rect(Vector2(a,b), Vector2(c,d)
	// a = Position in x; b = Position in y;
	// c = Resolution in "x"; d = Resolution in "y"
	// _layer from back [0] to front [255]

	
	// BACKGROUND
	m_ptr_skyDay = new Image("Assets\\Environment\\Sky_Day.png", Rect(Vector2(0, 0), Vector2(1920, 1080)), this, 0);
	m_ptr_firstLevelForestBackground = new Image("Assets\\Environment\\FirstLevelForestBackground.png", Rect(Vector2(0, 0), Vector2(1920, 1080)), this, 1);
	m_ptr_ground = new StaticObj("Assets\\Environment\\Ground.png", Rect(Vector2(0, 745), Vector2(1920, 385)), this, 5);

	// MAIN GAME
	m_ptr_smallBush = new Image("Assets\\Objects\\FirstLevelFindObject.png", Rect(Vector2(700, 550), Vector2(400, 200)), this, 2);
	m_ptr_afon = new Image("Assets\\Character\\Afon.png", Rect(Vector2(1300, 62), Vector2(600, 700)), this, 3);
	m_ptr_player = new Player(this, 4);
	m_ptr_firstLevelForegroundGrass = new Image("Assets\\Environment\\FirstLevelForegroundGrass.png", Rect(Vector2(0, 395), Vector2(1920, 500)), this, 6);

	// SEARCHING GAME
	m_ptr_searchingGameTrigger = new Trigger(Rect(m_ptr_smallBush->m_Position, m_ptr_smallBush->m_Resolution), this, 15);
	m_ptr_searchingGameBackground = new Image("Assets\\Environment\\SearchingGameBackground.png", Rect(Vector2(-10, -10), Vector2(0, 0)), this, 7);
	m_ptr_biggerBush = new Image("Assets\\Objects\\FirstLevelFindObject.png", Rect(Vector2(-50, -50), Vector2(0, 0)), this, 8);
	m_ptr_ballTrigger = new Trigger(Rect(Vector2(1115, 345), Vector2(50, 50)), this, 16);

	// MAZE GAME
	m_ptr_mazeGameBackground = new Image("Assets\\Objects\\Maze.png", Rect(Vector2(-10, -10), Vector2(0, 0)), this, 20);
	m_ptr_mazeBall = new DynamicObj("Assets\\Objects\\Ball.png", Rect(Vector2(-1000, -1000), Vector2(100, 100)), this, 21);
	m_ptr_mazeBall->m_MoveSpeed = 200;

	// ENDSCREEN
	m_ptr_winscreen = new Image("Assets\\Winscreen.png", Rect(Vector2(-10, 0), Vector2(0, 0)), this, 10);

	// UI
	m_ptr_buttonE = new Image("Assets\\UI\\ButtonE.png", Rect(Vector2(-100, -100), Vector2(100, 50)), this, 12);
	m_ptr_mission = new Image("Assets\\UI\\MissionMessage.png", Rect(Vector2(0, -1000), Vector2(1000, 100)), this, 13);
	m_ptr_winMessage = new Image("Assets\\UI\\WinMessage.png", Rect(Vector2(0, -1000), Vector2(1000, 100)), this, 14);

	// SOUND
	m_ptr_backgroundForrestMusic = new Audio("Assets\\Sound\\NatureAmbience.ogg", true);
	m_ptr_backgroundForrestMusic->Play();

	// OTHER
	m_ptr_vignetEffect = new Image("Assets\\Sky_SunsetVignette.png", Rect(Vector2(0, 0), Vector2(1920, 1080)), this, 11);
	m_ptr_triggerLeft = new Trigger(Rect(Vector2(0, 0), Vector2(20, 1080)), this, 18);
	m_ptr_triggerRight = new Trigger(Rect(Vector2(1900, 0), Vector2(20, 1080)), this, 19);

	//DIALOGUES
	m_timer = 6.0f;
	m_ptr_afonTrigger = new Trigger(Rect(m_ptr_afon->m_Position, m_ptr_afon->m_Resolution), this, 17);
	m_ptr_channingBubble1 = new Image("Assets\\Dialogue\\ChanningDialogue1.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(700, 350)), this, 9);
}


void HomecomingGame::Update(float _deltaTime)
{
	Speak(_deltaTime);

	SearchGame();

	MazeGame(_deltaTime);

	// Endscreen: player won
	if (m_winState)
	{
		m_ptr_player->m_ptr_ControlEnabled = false;
		m_ptr_vignetEffect->m_Resolution = Vector2(0, 0);
		m_ptr_winscreen->m_Position = Vector2(0, 0);
		m_ptr_winscreen->m_Resolution = Vector2(1920, 1080);
		m_ptr_winMessage->m_Position = Vector2(300, 0);
	}

	for (int i = 0; i < m_EntityList.size(); i++)
	{
		if (m_EntityList[i] != nullptr)
		{
			m_EntityList[i]->Update(_deltaTime);
		}
	}

	WorldBounds();
}
void HomecomingGame::Render(Renderer* _ptr_renderer)
{
	for (int i = 0; i < m_EntityList.size(); i++)
	{
		if (m_EntityList[i] != nullptr)
		{
			m_EntityList[i]->Render(_ptr_renderer);
		}
	}

#if _DEBUG
	for each (Collider* collider in m_ColliderList)
	{
		if (collider->m_IsTrigger)
		{
			_ptr_renderer->DrawTexture(m_ptr_triggerTexture, Rect(collider->m_Position, collider->m_Resolution));
		}
		else
		{
			_ptr_renderer->DrawTexture(m_ptr_colliderTexture, Rect(collider->m_Position, collider->m_Resolution));
		}
	}
#endif
}

// By Ramon
void HomecomingGame::Dialogue()
{
	if (m_ptr_dialogueStage < 15)
	{
		m_ptr_player->m_ptr_ControlEnabled = false;
		switch (m_ptr_dialogueStage)
		{
		case 0:
			m_ptr_channingBubble1->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble1 = new Image("Assets\\Dialogue\\AfonDialogue1.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing1 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 1:
			m_ptr_afonBubble1->m_Position = Vector2(-1000, 0);
			m_ptr_channingBubble2 = new Image("Assets\\Dialogue\\ChanningDialogue2.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon1 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 2:
			m_ptr_channingBubble2->m_Position = Vector2(-1000, 0);
			m_ptr_channingBubble3 = new Image("Assets\\Dialogue\\ChanningDialogue3.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(720, 370)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing2 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 3:
			m_ptr_channingBubble3->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble2 = new Image("Assets\\Dialogue\\AfonDialogue2.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing3 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 4:
			m_ptr_afonBubble2->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble3 = new Image("Assets\\Dialogue\\AfonDialogue3.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon2 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 5:
			m_ptr_afonBubble3->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble4 = new Image("Assets\\Dialogue\\AfonDialogue4.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon3 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 6:
			m_ptr_afonBubble4->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble5 = new Image("Assets\\Dialogue\\AfonDialogue5.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon4 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 7:
			m_ptr_afonBubble5->m_Position = Vector2(-10000, -2000);
			m_ptr_afonBubble6 = new Image("Assets\\Dialogue\\AfonDialogue6.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon5 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 8:
			m_ptr_afonBubble6->m_Position = Vector2(-10000, -5000);
			m_ptr_channingBubble4 = new Image("Assets\\Dialogue\\ChanningDialogue4.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon6 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 9:
			m_ptr_channingBubble4->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble7 = new Image("Assets\\Dialogue\\AfonDialogue7.png", Rect(Vector2(1050, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing4 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 10:
			m_ptr_afonBubble7->m_Position = Vector2(-1000, 0);
			m_ptr_channingBubble5 = new Image("Assets\\Dialogue\\ChanningDialogue5.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon7 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 11:
			m_ptr_channingBubble5->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble8 = new Image("Assets\\Dialogue\\afonDialogue8.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing5 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 12:
			m_ptr_afonBubble8->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble9 = new Image("Assets\\Dialogue\\afonDialogue9.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon8 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 13:
			m_ptr_afonBubble9->m_Position = Vector2(-1000, 0);
			m_ptr_channingBubble6 = new Image("Assets\\Dialogue\\ChanningDialogue6.png", Rect(Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Afon9 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		case 14:
			m_ptr_channingBubble6->m_Position = Vector2(-1000, 0);
			m_ptr_afonBubble10 = new Image("Assets\\Dialogue\\afonDialogue10.png", Rect(Vector2(900, 50), Vector2(700, 350)), this, 9);
			m_ptr_dialogueStage++;
			//printf("Channing6 gone, dialogStage = " + m_ptr_dialogueStage);
			break;
		}
	}
	else
	{
		m_ptr_afonBubble10->m_Position = Vector2(-1000, 0);
		//printf("Afon10 gone, dialogStage = " + m_ptr_dialogueStage);
		m_ptr_player->m_ptr_ControlEnabled = true;
	}
}

void HomecomingGame::Speak(float _deltaTime)
{
	// Dialogue: Start Message
	if (m_ptr_channingBubble1 != nullptr)
	{
		m_timer -= _deltaTime;
		if (m_timer > 0)
		{
			m_ptr_channingBubble1->m_Position = Vector2(m_ptr_player->m_Position.m_X - 60, m_ptr_player->m_Position.m_Y - 200);
		}
		else
		{
			m_ptr_channingBubble1->m_Position = Vector2(-1000, 0);
			// delete m_ptr_channingBubble1;
			// m_ptr_channingBubble1 = nullptr;
		}
	}

	// Dialogue: player speaks with afon
	if (m_ptr_player->m_ptr_physics->m_ptr_Collider->GetCollisionState(m_ptr_afonTrigger->m_ptr_Physics->m_ptr_Collider)
		&& !m_searchGame)
	{
		m_dialogueEncounter = true;
		m_ptr_buttonE->m_Position = Vector2(m_ptr_player->m_Position.m_X + 200, m_ptr_player->m_Position.m_Y - 20);
		if (Input::KeyReleased(SDL_Scancode::SDL_SCANCODE_E))
		{
			Dialogue();
		}
	}
	else
	{
		m_dialogueEncounter = false;
	}

	if (m_ptr_dialogueStage >= 15
		&& !m_searchGame)
	{
		m_ptr_mission->m_Position = Vector2(300, 0);
	}
	else
	{
		m_ptr_mission->m_Position = Vector2(0, -1000);
	}
}

void HomecomingGame::SearchGame()
{
	// SearchingGame: start game when player presses E near the bush
	if (m_ptr_player->m_ptr_physics->m_ptr_Collider->GetCollisionState(m_ptr_searchingGameTrigger->m_ptr_Physics->m_ptr_Collider)
		&& !m_bushminigame
		&& !m_searchGame)
	{
		m_ptr_buttonE->m_Position = Vector2(m_ptr_player->m_Position.m_X + 200, m_ptr_player->m_Position.m_Y - 20);
		if (Input::KeyReleased(SDL_Scancode::SDL_SCANCODE_E))
		{
			m_bushminigame = true;
			m_ptr_player->m_ptr_ControlEnabled = false;
			m_ptr_buttonE->m_Position = Vector2(-100, 0);
			m_ptr_searchingGameBackground->m_Position = Vector2(0, 0);
			m_ptr_searchingGameBackground->m_Resolution = Vector2(1920, 1080);
			m_ptr_biggerBush->m_Position = Vector2(0, 0);
			m_ptr_biggerBush->m_Resolution = Vector2(1920, 1080);
		}
	}
	else if (!m_dialogueEncounter)
	{
		m_ptr_buttonE->m_Position = Vector2(-100, 0);
	}

	if (m_bushminigame && Input::KeyReleased(SDL_Scancode::SDL_SCANCODE_E))
	{
		return;
	}

	// SearchingGame: check whether player clicked on the ball
	if (m_bushminigame && Input::MouseDown(0)
		&& !(Input::GetMousePosition().m_X >= m_ptr_ballTrigger->m_Position.m_X + m_ptr_ballTrigger->m_Resolution.m_X
			|| Input::GetMousePosition().m_X <= m_ptr_ballTrigger->m_Position.m_X
			|| Input::GetMousePosition().m_Y >= m_ptr_ballTrigger->m_Position.m_Y + m_ptr_ballTrigger->m_Resolution.m_Y
			|| Input::GetMousePosition().m_Y <= m_ptr_ballTrigger->m_Position.m_Y))
	{
		m_searchGame = true;
		m_ptr_player->m_ptr_ControlEnabled = true;
		m_ptr_searchingGameBackground->m_Position = Vector2(-10, -10);
		m_ptr_searchingGameBackground->m_Resolution = Vector2(0, 0);
		m_ptr_biggerBush->m_Position = Vector2(-10, -10);
		m_ptr_biggerBush->m_Resolution = Vector2(0, 0);
		m_bushminigame = false;
	}
}

void HomecomingGame::MazeGame(float _deltaTime)
{
	if (m_ptr_player->m_ptr_physics->m_ptr_Collider->GetCollisionState(m_ptr_triggerRight->m_ptr_Physics->m_ptr_Collider))
	{
		//m_ptr_player->m_ptr_physics->m_colliderEnabled = false;
		//m_ptr_ground->m_ptr_physics->m_colliderEnabled = false;
		m_ptr_player->m_Position = Vector2(-1000, -1000);
		m_ptr_ground->m_Position = Vector2(-10000, -10000);
		m_ptr_player->m_ptr_ControlEnabled = false;
		m_ptr_mazeGameBackground->m_Position = Vector2(0, 0);
		m_ptr_mazeGameBackground->m_Resolution = Vector2(1920, 1080);

		m_ptr_mazeWall1 = new StaticObj("", Rect(Vector2(0, 275), Vector2(600, 60)), this, 22);
		m_ptr_mazeWall2 = new StaticObj("", Rect(Vector2(600, 275), Vector2(70, 270)), this, 23);
		m_ptr_mazeWall3 = new StaticObj("", Rect(Vector2(300, 110), Vector2(1350, 50)), this, 24);
		m_ptr_mazeWall4 = new StaticObj("", Rect(Vector2(1650, 110), Vector2(50, 970)), this, 25);
		m_ptr_mazeWall5 = new StaticObj("", Rect(Vector2(270, 550), Vector2(65, 240)), this, 26);
		m_ptr_mazeWall6 = new StaticObj("", Rect(Vector2(270, 790), Vector2(700, 60)), this, 27);
		m_ptr_mazeWall7 = new StaticObj("", Rect(Vector2(970, 380), Vector2(60, 470)), this, 28);
		m_ptr_mazeWall8 = new StaticObj("", Rect(Vector2(1030, 380), Vector2(270, 60)), this, 29);
		m_ptr_mazeWall9 = new StaticObj("", Rect(Vector2(1300, 380), Vector2(60, 700)), this, 30);
		m_ptr_mazeWall10 = new StaticObj("", Rect(Vector2(0, 0), Vector2(1920, 10)), this, 31);
		m_ptr_mazeWall11 = new StaticObj("", Rect(Vector2(0, 1070), Vector2(1920, 10)), this, 32);
		m_ptr_mazeWall12 = new StaticObj("", Rect(Vector2(0, 0), Vector2(10, 1080)), this, 33);
		m_ptr_mazeWall13 = new StaticObj("", Rect(Vector2(1910, 0), Vector2(10, 1080)), this, 34);

		if (m_searchGame)
		{
			m_ptr_mazeBall->m_Position = Vector2(10, 400);
		}

		if (Input::KeyReleased(SDL_Scancode::SDL_SCANCODE_E))
		{
			return;
		}
	}
		
	if (m_searchGame)
	{
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_W) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_UP))
		{
			m_ptr_mazeBall->m_Speed.m_Y = -m_ptr_mazeBall->m_MoveSpeed;
		}
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_A) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_LEFT))
		{
			m_ptr_mazeBall->m_Speed.m_X = -m_ptr_mazeBall->m_MoveSpeed;
		}
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_D) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_RIGHT))
		{
			m_ptr_mazeBall->m_Speed.m_X = m_ptr_mazeBall->m_MoveSpeed;
		}
		if (Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_S) || Input::KeyPressed(SDL_Scancode::SDL_SCANCODE_DOWN))
		{
			m_ptr_mazeBall->m_Speed.m_Y = m_ptr_mazeBall->m_MoveSpeed;
		}

		if (Input::KeyReleased(SDL_Scancode::SDL_SCANCODE_E))
		{
			return;
		}
	}
}

void HomecomingGame::WorldBounds()
{
	// WorldBounds: make sure player stays in world
	if (m_ptr_player->m_ptr_physics->m_ptr_Collider->GetCollisionState(m_ptr_triggerLeft->m_ptr_Physics->m_ptr_Collider))
	{
		m_ptr_player->m_Position.m_X = 1670;
	}
	if (m_ptr_player->m_ptr_physics->m_ptr_Collider->GetCollisionState(m_ptr_triggerRight->m_ptr_Physics->m_ptr_Collider))
	{
		m_ptr_player->m_Position.m_X = -70;
	}
}