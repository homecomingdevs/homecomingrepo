#include "Physics.h"
#include "Collider.h"
#include "Entity.h"

Physics::Physics(Entity* _ptr_entity)
{
	m_ptr_Entity = _ptr_entity;
}

void Physics::AddGravity(float _acceleration)
{
	this->m_ptr_gravity = new Gravity(_acceleration);
	m_gravityEnabled = true;
}
void Physics::AddCollider(Vector2 _offset, Vector2 _resolution, bool _isDynamic, bool _isTrigger)
{
	this->m_ptr_Collider = new Collider(_offset, _resolution, _isDynamic, _isTrigger, this);
	m_colliderEnabled = true;
}

void Physics::PUpdate(float _deltaTime)
{
	if (m_gravityEnabled)
	{
		m_ptr_gravity->GUpdate(m_ptr_Entity, _deltaTime);
	}
	m_ptr_Entity->m_Position.Add(Vector2(m_ptr_Entity->m_Speed.m_X * _deltaTime, m_ptr_Entity->m_Speed.m_Y * _deltaTime));
	if (m_colliderEnabled)
	{
		m_ptr_Collider->CUpdate();
	}
}

Physics::~Physics()
{
	m_ptr_Entity->m_ptr_Game->m_ColliderList.remove(m_ptr_Collider);
}