#include "Collider.h"
#include "Physics.h"
#include <math.h>

Collider::Collider(Vector2 _offset, Vector2 _resolution, bool _isDynamic, bool _isTrigger, Physics* _ptr_physics)
{
	Vector2 temp;
	m_ptr_physics = _ptr_physics;
	m_ptr_entity = _ptr_physics->m_ptr_Entity;
	m_offset = _offset;
	temp = m_ptr_entity->m_Position;
	m_Position = temp.Add(m_offset);
	m_prevPos = m_Position;
	m_Resolution = _resolution;
	m_isDynamic = _isDynamic;
	m_IsTrigger = _isTrigger;
	m_ptr_physics->m_ptr_Entity->m_ptr_Game->m_ColliderList.push_back(this);
}
Collider::~Collider()
{
	m_ptr_entity->m_ptr_Game->m_ColliderList.remove(this);
}

Vector2 Collider::IntersectRect(Rect _rectA, Rect _rectB)
{
	float halfWidthA = _rectA.w / 2.0f;
	float halfHeightA = _rectA.h / 2.0f;
	float halfWidthB = _rectB.w / 2.0f;
	float halfHeightB = _rectB.h / 2.0f;

	Vector2 centerA = Vector2(_rectA.x + halfWidthA, _rectA.y + halfHeightA);
	Vector2 centerB = Vector2(_rectB.x + halfWidthB, _rectB.y + halfHeightB);

	float distanceX = centerA.m_X - centerB.m_X;
	float distanceY = centerA.m_Y - centerB.m_Y;
	float minDistanceX = halfWidthA + halfWidthB;
	float minDistanceY = halfHeightA + halfHeightB;

	if (fabs(distanceX) >= minDistanceX || fabs(distanceY) >= minDistanceY)
	{
		return Vector2(0, 0);
	}

	float depthX = distanceX > 0 ? minDistanceX - distanceX + 1 : -minDistanceX - distanceX - 1;
	float depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
	return Vector2(depthX, depthY);
}

bool Collider::GetCollisionState()
{
	for each (Collider* collider in m_ptr_physics->m_ptr_Entity->m_ptr_Game->m_ColliderList)
	{
		if (!(collider == this)
			&& GetCollisionState(collider))
		{
			return true;
		}
	}
}
bool Collider::GetCollisionState(Collider* _ptr_collider)
{
	if (!_ptr_collider->m_ptr_physics->m_colliderEnabled)
	{
		return false;
	}
	if ((this->m_Position.m_X + this->m_Resolution.m_X <= _ptr_collider->m_Position.m_X
		|| this->m_Position.m_X >= _ptr_collider->m_Position.m_X + _ptr_collider->m_Resolution.m_X
		|| this->m_Position.m_Y >= _ptr_collider->m_Position.m_Y + _ptr_collider->m_Resolution.m_Y
		|| this->m_Position.m_Y + this->m_Resolution.m_Y <= _ptr_collider->m_Position.m_Y))
	{
		m_ptr_ColliderHit = nullptr;
		return false;
	}
	else
	{
		m_ptr_ColliderHit = _ptr_collider;
		if (m_prevPos.m_X > (m_ptr_ColliderHit->m_Position.m_X + m_ptr_ColliderHit->m_Resolution.m_X)
			|| (m_prevPos.m_X + m_Resolution.m_X) < m_ptr_ColliderHit->m_Position.m_X)
		{
			m_overlappedX = false;
		}
		else
		{
			m_overlappedX = true;
		}
		if (m_prevPos.m_Y > (m_ptr_ColliderHit->m_Position.m_Y + m_ptr_ColliderHit->m_Resolution.m_Y)
			|| (m_prevPos.m_Y + m_Resolution.m_Y) < m_ptr_ColliderHit->m_Position.m_Y)
		{
			m_overlappedY = false;
		}
		else
		{
			m_overlappedY = true;
		}
		m_overlap = IntersectRect(Rect(m_Position, m_Resolution), Rect(m_ptr_ColliderHit->m_Position,m_ptr_ColliderHit->m_Resolution));
		return true;
	}
}
void Collider::CollisionResponse()
{
	if (m_prevPos.m_Y <= m_Position.m_Y)
	{
		m_IsGrounded = true;
	}
	if (!m_overlappedX
		&& m_overlappedY)
	{
		m_ptr_entity->m_Speed.m_X = fmin(m_ptr_entity->m_Speed.m_X, 0.0);
		m_Position.m_X += m_overlap.m_X;
		return;
	}
	else//if (!m_overlappedY
		//	&& m_overlappedX)
	{
		m_ptr_entity->m_Speed.m_Y = fmin(m_ptr_entity->m_Speed.m_Y, 0.0);
		m_Position.m_Y += m_overlap.m_Y;
		return;
	}
}
void Collider::CUpdate()
{
	Vector2 temp;
	temp = m_ptr_entity->m_Position;
	m_IsGrounded = false;
	m_prevPos = m_Position;
	m_Position = temp.Add(m_offset);
	if (m_isDynamic
		&& !m_IsTrigger
		&& GetCollisionState()
		&& m_ptr_ColliderHit != nullptr
		&& !m_ptr_ColliderHit->m_IsTrigger)
	{
		CollisionResponse();

		Vector2 temp;
		temp = m_Position;
		m_ptr_entity->m_Position = temp.Add(Vector2(m_offset.m_X * -1, m_offset.m_Y * -1));
	}
}