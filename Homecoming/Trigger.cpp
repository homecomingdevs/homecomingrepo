#include "Trigger.h"

Trigger::Trigger(Rect _posRes, Game* _ptr_game, int _layer) : Entity(_ptr_game, _layer)
{
	m_Position = Vector2(_posRes.x, _posRes.y);
	m_Resolution = Vector2(_posRes.w, _posRes.h);
	m_ptr_Physics = new Physics(this);
	m_ptr_Physics->AddCollider(Vector2(0, 0), m_Resolution, false, true);
}