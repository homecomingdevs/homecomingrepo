#pragma once
#include "SDLInclude.h"
#include "Vector2.h"

class Input
{
private:
	/// <value> Saves keys pressed in current frame
	/// </value>
	static bool m_keyIsDown[256];
	/// <value> Saves keys pressed in previous frame
	/// </value>
	static bool m_keyWasDown[256];

	/// <value> Saves mouse buttons pressed in current frame
	/// </value>
	static bool m_mouseIsDown[3];
	/// <value> Saves mouse buttons pressed in previous frame
	/// </value>
	static bool m_mouseWasDown[3];

	static int m_mouseXPos;
	static int m_mouseYPos;

public:
	static Vector2 GetMousePosition();
	void ParseEvent(SDL_Event _ptr_event);
	void FlushKeys();

	/// <summary> Returns wether the key is pressed in current frame
	/// </summary>
	static bool KeyDown(SDL_Scancode _ptr_keyCode);
	/// <summary> Returns wether the key is held down in previous and current frame
	/// </summary>
	static bool KeyPressed(SDL_Scancode _ptr_keyCode);
	/// <summary> Returns wether the key is released in current frame
	/// </summary>
	static bool KeyReleased(SDL_Scancode _ptr_keyCode);

	/// <summary> Returns wether the mouse button is pressed in current frame
	/// </summary>
	static bool MouseDown(int _button);
	/// <summary> Returns wether the mouse button is held down in previous and current frame
	/// </summary>
	static bool MousePressed(int _button);
	/// <summary> Returns wether the mouse button is released in current frame
	/// </summary>
	static bool MouseReleased(int _button);
};