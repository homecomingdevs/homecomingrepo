#pragma once

#include <SDL_ttf.h>


class Font
{
public:
	TTF_Font* m_Font;

	///<summary> Imports a font with specified size
	///</summary>
	Font(char* _filename, int _size);
	~Font();
};