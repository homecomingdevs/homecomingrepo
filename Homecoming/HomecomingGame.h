#pragma once
#include "Renderer.h"
#include "Game.h"
#include "StaticObj.h"
#include "Image.h"
#include "Player.h"
#include "Trigger.h"
#include "Audio.h"
#include "DynamicObj.h"

#if _DEBUG
#include "Texture.h"
#endif

// Pay attenion to the comments in the game class
class HomecomingGame : public Game
{
private:
#if _DEBUG
	Texture* m_ptr_colliderTexture;
	Texture* m_ptr_triggerTexture;
#endif

	
	//BACKGROUND
	Image* m_ptr_skyDay;
	Image* m_ptr_firstLevelForestBackground;
	StaticObj* m_ptr_ground;

	// MAIN GAME
	Image* m_ptr_smallBush;
	Player* m_ptr_player;
	Image* m_ptr_afon;
	Image* m_ptr_firstLevelForegroundGrass;

	// SEARCHING GAME
	Trigger* m_ptr_searchingGameTrigger;
	Image* m_ptr_searchingGameBackground;
	Image* m_ptr_biggerBush;
	Trigger* m_ptr_ballTrigger;

	// MAZE GAME
	Image* m_ptr_mazeGameBackground;
	DynamicObj* m_ptr_mazeBall;
	StaticObj* m_ptr_mazeWall1;
	StaticObj* m_ptr_mazeWall2;
	StaticObj* m_ptr_mazeWall3;
	StaticObj* m_ptr_mazeWall4;
	StaticObj* m_ptr_mazeWall5;
	StaticObj* m_ptr_mazeWall6;
	StaticObj* m_ptr_mazeWall7;
	StaticObj* m_ptr_mazeWall8;
	StaticObj* m_ptr_mazeWall9;
	StaticObj* m_ptr_mazeWall10;
	StaticObj* m_ptr_mazeWall11;
	StaticObj* m_ptr_mazeWall12;
	StaticObj* m_ptr_mazeWall13;


	// ENDSCREEN
	Image* m_ptr_winscreen;

	// UI
	Image* m_ptr_buttonE;
	Image* m_ptr_mission;
	Image* m_ptr_winMessage;

	// SOUND
	Audio* m_ptr_backgroundForrestMusic;

	// OTHER
	Image* m_ptr_vignetEffect;
	Trigger* m_ptr_triggerLeft;
	Trigger* m_ptr_triggerRight;
	bool m_bushminigame;
	bool m_dialogueEncounter;
	bool m_searchGame;
	bool m_winState;

	//DIALOGUE
	float m_timer;
	Trigger* m_ptr_afonTrigger;
	int m_ptr_dialogueStage = 0;
	Image* m_ptr_afonBubble1;
	Image* m_ptr_afonBubble2;
	Image* m_ptr_afonBubble3;
	Image* m_ptr_afonBubble4;
	Image* m_ptr_afonBubble5;
	Image* m_ptr_afonBubble6;
	Image* m_ptr_afonBubble7;
	Image* m_ptr_afonBubble8;
	Image* m_ptr_afonBubble9;
	Image* m_ptr_afonBubble10;
	Image* m_ptr_channingBubble1;
	Image* m_ptr_channingBubble2;
	Image* m_ptr_channingBubble3;
	Image* m_ptr_channingBubble4;
	Image* m_ptr_channingBubble5;
	Image* m_ptr_channingBubble6;
	


public:

	void Initialize(Renderer* _ptr_renderer) override;
	void Update(float _deltaTime) override;
	void Render(Renderer* _ptr_renderer) override;

	void Dialogue();
	void Speak(float _deltaTime);
	void SearchGame();
	void MazeGame(float _deltaTime);
	void WorldBounds();
};