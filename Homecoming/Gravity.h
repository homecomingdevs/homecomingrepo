#pragma once
#include "Entity.h"

class Gravity
{
private:
	float m_acceleration;

public:
	Gravity(float _acceleration);

	void GUpdate(Entity* _ptr_entity, float _deltaTime);
};