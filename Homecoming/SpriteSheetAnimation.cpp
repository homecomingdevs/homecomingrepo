#include "SpriteSheetAnimation.h"
#include "Texture.h"

SpriteSheetAnimation::SpriteSheetAnimation(char* _filename, float _spf, Vector2 _frameResolution, Renderer* _ptr_renderer)
{
	m_ptr_renderer = _ptr_renderer;
	m_ptr_texture = new Texture(_filename, _ptr_renderer);
	m_currentFrame = 0;
	m_spf = _spf;
	m_deltaSpf = _spf;
	m_frameResolution = _frameResolution;
}
SpriteSheetAnimation::~SpriteSheetAnimation()
{
	delete m_ptr_texture;
	m_ptr_texture = nullptr;
}

void SpriteSheetAnimation::AUpdate(float _deltaTime)
{
	if (m_ptr_CurrentAnimation != nullptr)
	{
		m_deltaSpf -= _deltaTime;
		if (m_deltaSpf * 1000.0f <= 0.0f)
		{
			m_deltaSpf = m_spf;
			m_currentFrame++;
		}
	}
	if (m_currentFrame >= m_ptr_CurrentAnimation->m_Frames)
	{
		m_currentFrame = 0;
	}
}
void SpriteSheetAnimation::ADraw(Vector2 _position, Vector2 _resolution)
{
	if (m_ptr_CurrentAnimation != nullptr)
	{
		m_ptr_renderer->DrawTexture(m_ptr_texture, Rect(Vector2((m_ptr_CurrentAnimation->m_Column + m_currentFrame) * m_frameResolution.m_X,
			m_ptr_CurrentAnimation->m_Line * m_frameResolution.m_Y), m_frameResolution), Rect(_position, _resolution));
	}
}