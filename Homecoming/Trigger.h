#pragma once
#include "Entity.h"
#include "Physics.h"
#include "Game.h"

class Trigger : public Entity
{
public:
	Physics* m_ptr_Physics;

	Trigger(Rect _posRes, Game* _ptr_game, int _layer);
};