#include <iostream>
#include <SDL_image.h>
#include "Texture.h"
#include "Renderer.h"

Texture::Texture(char* _ptr_filename, Renderer* _ptr_renderer)
{
	if (_ptr_filename != "")
	{
		SDL_Surface* surface = IMG_Load(_ptr_filename);
		m_ptr_texture = SDL_CreateTextureFromSurface(_ptr_renderer->GetRenderer(), surface);
		if (m_ptr_texture == nullptr)
		{
			std::cout << "\nError while loading texture " << _ptr_filename << ": " << IMG_GetError() << "\n";
		}
	}
}
Texture::~Texture()
{
	SDL_DestroyTexture(m_ptr_texture);
}

SDL_Texture* Texture::GetTexture()
{
	return m_ptr_texture;
}