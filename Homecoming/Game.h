#pragma once
#include "Renderer.h"
#include <list>
#include "Collider.h"
#include <array>

class Entity;

class Game
{
public:
	std::array<Entity*,256> m_EntityList;
	std::list<Collider*> m_ColliderList;
	Renderer* m_ptr_Renderer;
	bool m_isRunning;

	/// <summary> Called once upon start of the game
	/// </summary>
	virtual void Initialize(Renderer* _ptr_renderer);
	/// <summary> Called once per frame
	/// </summary>
	virtual void Update(float _ptr_deltaTime);
	/// <summary> Renders frame after Update()
	/// </summary>
	virtual void Render(Renderer* _ptr_renderer);
};
