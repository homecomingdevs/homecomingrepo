#include "StaticObj.h"

StaticObj::StaticObj(char* _ptr_filename, Rect _posRes, Game* _ptr_game, int _layer) : Entity(_ptr_game, _layer)
{
	m_Position = Vector2(_posRes.x, _posRes.y);
	m_Resolution = Vector2(_posRes.w, _posRes.h);
	m_ptr_texture = new Texture(_ptr_filename, _ptr_game->m_ptr_Renderer);
	m_ptr_physics = new Physics(this);
	m_ptr_physics->AddCollider(Vector2(0, 0), m_Resolution, false, false);
}
StaticObj::~StaticObj()
{
	delete m_ptr_texture;
	m_ptr_texture = nullptr;
}

void StaticObj::Update(float _deltaTime)
{
	m_ptr_physics->PUpdate(_deltaTime);
}

void StaticObj::Render(Renderer* _ptr_renderer)
{
	_ptr_renderer->DrawTexture(m_ptr_texture, Rect(m_Position, m_Resolution));
}