#pragma once
#include "Gravity.h"
#include <list>
#include "Game.h"

class Collider;
class Entity;

class Physics
{
private:
	Gravity* m_ptr_gravity;
	bool m_gravityEnabled;

	bool m_collisionTop;
	bool m_collisionBottom;
	bool m_collisionLeft;
	bool m_collisionRight;

public:
	bool m_colliderEnabled;
	Collider* m_ptr_Collider;
	Entity* m_ptr_Entity;

	Physics(Entity* _ptr_entity);
	~Physics();

	void AddGravity(float _acceleration);
	void AddCollider(Vector2 _offset, Vector2 _resolution, bool _isDynamic, bool _isTrigger);
	///<summary> Updates physics of parent entity once per frame
	///</summary>
	void PUpdate(float _deltaTime);
};