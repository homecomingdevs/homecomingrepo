#pragma once
#include "Entity.h"
#include "Texture.h"
#include "Game.h"
#include "Renderer.h"
#include "Physics.h"
#include "Input.h"
#include "SpriteSheetAnimation.h"
#include "Audio.h"

class Player : public Entity
{
private:
	int m_moveSpeed;
	int m_jumpSpeed;
	AnimationClip* m_ptr_idle;
	AnimationClip* m_ptr_idleRight;
	AnimationClip* m_ptr_idleLeft;
	AnimationClip* m_ptr_walkRight;
	AnimationClip* m_ptr_walkLeft;
	SpriteSheetAnimation* m_ptr_anim;
	Audio* m_ptr_walking;

public:
	Physics* m_ptr_physics;
	bool m_ptr_ControlEnabled;

	Player(Game* _ptr_game, int _layer);
	
	void Update(float _deltaTime) override;
	void Render(Renderer* _ptr_renderer) override;
};