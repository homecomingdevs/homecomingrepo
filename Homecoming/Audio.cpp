#include "Audio.h"
#include <SDL_mixer.h>
#include <iostream>

Audio::Audio(char* _filename, bool _isMusic)
{
	m_isMusic = _isMusic;
	if (m_isMusic)
	{
		m_music = Mix_LoadMUS(_filename);
		if (m_music == nullptr)
		{
			std::cout << "\nError while loading " << _filename << ": " << Mix_GetError() << "\n";
		}
	}
	else
	{
		m_chunk = Mix_LoadWAV(_filename);
		if (m_chunk == nullptr)
		{
			std::cout << "\nError while loading " << _filename << ": " << Mix_GetError() << "\n";
		}
	}
}
Audio::~Audio()
{
	Mix_FreeMusic(m_music);
	Mix_FreeChunk(m_chunk);
}

void Audio::Play()
{
	if (m_isMusic)
	{
		// Is music already playing
		if (Mix_PlayingMusic() == 0)
		{
			Mix_PlayMusic(m_music, -1);
		}
		else
		{
			if (Mix_PausedMusic() == 1)
			{
				Mix_ResumeMusic();
			}
			else
			{
				Mix_PauseMusic();
			}
		}
	}
	else
	{
		Mix_PlayChannel(1, m_chunk, -1);
	}
}
void Audio::Pause()
{
	if (m_isMusic)
	{
		Mix_PauseMusic();
	}
	else
	{
		Mix_Pause(1);
	}
}
void Audio::Resume()
{
	if (m_isMusic)
	{
		Mix_ResumeMusic();
	}
	else
	{
		Mix_Resume(1);
	}
}
void Audio::Stop()
{
	if (m_isMusic)
	{
		Mix_HaltMusic();
	}
	else
	{
		Mix_HaltChannel(1);
	}
}