#pragma once
#include "Vector2.h"
#include "Renderer.h"

class Game;

// Pay attention to the comments in the update and render functions
class Entity
{
private:
	int m_layer;
public:
	Game* m_ptr_Game;
	Vector2 m_Position;
	Vector2 m_Resolution;
	Vector2 m_Speed;

	Entity(Game* _ptr_game, int _layer);
	virtual ~Entity();

	///<summary> Called once per frame
	///</summary>
	virtual void Update(float _deltaTime);
	///<summary> Renders all rendermethods for this specific entity
	///</summary>
	virtual void Render(Renderer* _ptr_renderer);
};