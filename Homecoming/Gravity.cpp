#include "Gravity.h"
#include <math.h>

Gravity::Gravity(float _acceleration)
{
	m_acceleration = _acceleration;
}

void Gravity::GUpdate(Entity* _ptr_entity, float _deltaTime)
{
		_ptr_entity->m_Speed.m_Y += (int)fminf(m_acceleration * _deltaTime, 500.0f);
}