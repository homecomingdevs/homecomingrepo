#include "DynamicObj.h"

DynamicObj::DynamicObj(char* _ptr_filename, Rect _posRes, Game* _ptr_game, int _layer) : Entity(_ptr_game, _layer)
{
	m_Position = Vector2(_posRes.x, _posRes.y);
	m_Resolution = Vector2(_posRes.w, _posRes.h);
	m_ptr_texture = new Texture(_ptr_filename, _ptr_game->m_ptr_Renderer);
	m_ptr_physics = new Physics(this);
	m_ptr_physics->AddCollider(Vector2(0, 0), m_Resolution, true, false);
}

DynamicObj::~DynamicObj()
{
	delete m_ptr_texture;
	m_ptr_texture = nullptr;
	delete m_ptr_physics;
	m_ptr_physics = nullptr;
}

void DynamicObj::Update(float _deltaTime)
{
	this->m_ptr_physics->PUpdate(_deltaTime);
	m_Speed = Vector2(0, 0);
}

void DynamicObj::Render(Renderer* _ptr_renderer)
{
	_ptr_renderer->DrawTexture(m_ptr_texture, Rect(m_Position, m_Resolution));
}