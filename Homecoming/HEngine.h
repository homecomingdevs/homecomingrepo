#pragma once
#include <time.h>
#include "SDLInclude.h"
#include "Renderer.h"
#include "Input.h"
#include "Game.h"

class HEngine
{
private:
	Renderer* m_ptr_renderer;
	SDL_Window* m_ptr_window;
	Input* m_ptr_input;
	float m_lastTime;
	float m_currentTime;
	float m_deltaTime;

public:
	void Initialize(char* _ptr_windowName, Rect _windowRect);
	void Run(Game* _ptr_game);
	void Finalize();
};