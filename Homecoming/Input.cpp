#include "Input.h"

bool Input::m_keyIsDown[];
bool Input::m_keyWasDown[];

bool Input::m_mouseIsDown[];
bool Input::m_mouseWasDown[];

int Input::m_mouseXPos;
int Input::m_mouseYPos;

Vector2 Input::GetMousePosition()
{
	return Vector2(m_mouseXPos, m_mouseYPos);
}
void Input::FlushKeys()
{
	Uint32 mouseState = SDL_GetMouseState(&m_mouseXPos, &m_mouseYPos);

	if ((mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)) > 0)
	{
		m_mouseIsDown[0] = true;
	}
	else
	{
		m_mouseIsDown[0] = false;
	}
	if ((mouseState & SDL_BUTTON(SDL_BUTTON_MIDDLE)) > 0)
	{
		m_mouseIsDown[1] = true;
	}
	else
	{
		m_mouseIsDown[1] = false;
	}
	if ((mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT)) > 0)
	{
		m_mouseIsDown[2] = true;
	}
	else
	{
		m_mouseIsDown[2] = false;
	}

	for (int i = 0; i < 256; i++)
	{
		m_keyWasDown[i] = m_keyIsDown[i];
	}
	for (int i = 0; i < 3; i++)
	{
		m_keyWasDown[i] = m_keyIsDown[i];
	}
}
void Input::ParseEvent(SDL_Event _ptr_event)
{
	if (_ptr_event.type == SDL_EventType::SDL_KEYDOWN)
	{
		m_keyIsDown[_ptr_event.key.keysym.scancode] = true;
	}
	else
	{
		m_keyIsDown[_ptr_event.key.keysym.scancode] = false;
	}
}

bool Input::KeyDown(SDL_Scancode _ptr_keyCode)
{
	return m_keyIsDown[_ptr_keyCode];
}
bool Input::KeyPressed(SDL_Scancode _ptr_keyCode)
{
	return m_keyIsDown[_ptr_keyCode] && m_keyWasDown[_ptr_keyCode];
}
bool Input::KeyReleased(SDL_Scancode _ptr_keyCode)
{
	return !m_keyIsDown[_ptr_keyCode] && m_keyWasDown[_ptr_keyCode];
}

bool Input::MouseDown(int _button)
{
	return m_mouseIsDown[_button];
}
bool Input::MousePressed(int _button)
{
	return m_mouseIsDown[_button] && !m_mouseWasDown[_button];
}
bool Input::MouseReleased(int _button)
{
	return !m_mouseIsDown[_button] && m_mouseWasDown[_button];
}