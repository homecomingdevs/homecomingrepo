#include <iostream>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include "HEngine.h"
#include "Entity.h"

using namespace std;

void HEngine::Initialize(char* _ptr_windowName, Rect _windowRect)
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		cout << "\nError while initializing SDL\n";
		return;
	}

	m_ptr_window = SDL_CreateWindow(_ptr_windowName, _windowRect.x, _windowRect.y, _windowRect.w, _windowRect.h, SDL_WindowFlags::SDL_WINDOW_SHOWN /*| SDL_WindowFlags::SDL_WINDOW_FULLSCREEN*/);
	if (m_ptr_window == nullptr)
	{
		cout << "\nError while initializing window\n";
		return;
	}

	m_ptr_renderer = new Renderer(m_ptr_window);
	m_ptr_input = new Input();

	if (IMG_Init(IMG_InitFlags::IMG_INIT_JPG | IMG_InitFlags::IMG_INIT_PNG) != (IMG_InitFlags::IMG_INIT_JPG | IMG_InitFlags::IMG_INIT_PNG))
	{
		cout << "\nError while initializing SDL_image\n";
		return;
	}
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		cout << "\nError while initializing SDL_mixer\n";
		return;
	}
}
void HEngine::Run(Game* _ptr_game)
{

	_ptr_game->Initialize(m_ptr_renderer);
	m_lastTime = SDL_GetTicks();

	while (_ptr_game->m_isRunning)
	{
		m_ptr_input->FlushKeys();
		SDL_Event sdlEvent;

		while (SDL_PollEvent(&sdlEvent) == 1)
		{
			if (sdlEvent.type == SDL_EventType::SDL_QUIT)
			{
				return;
			}
			if (sdlEvent.type == SDL_EventType::SDL_KEYDOWN || sdlEvent.type == SDL_EventType::SDL_KEYUP)
			{
				m_ptr_input->ParseEvent(sdlEvent);
			}
			if (m_ptr_input->KeyDown(SDL_Scancode::SDL_SCANCODE_ESCAPE))
			{
				return;
			}
		}

		m_currentTime = SDL_GetTicks();
		if (m_currentTime > m_lastTime)
		{
			m_deltaTime = (m_currentTime - m_lastTime) / 1000.0f;
		}
		else
		{
			m_deltaTime = 0.0f;
		}
		m_lastTime = m_currentTime;

		_ptr_game->Update(m_deltaTime);

		m_ptr_renderer->Clear();
		_ptr_game->Render(m_ptr_renderer);
		m_ptr_renderer->Present();
	}
}
void HEngine::Finalize()
{
	TTF_Quit();
	Mix_CloseAudio();
	SDL_Quit();
}