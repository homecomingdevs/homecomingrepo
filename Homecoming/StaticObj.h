#pragma once
#include "Entity.h"
#include "Texture.h"
#include "Physics.h"

class StaticObj : public Entity
{
private:
	Texture* m_ptr_texture;

public:
	Physics* m_ptr_physics;

	StaticObj(char* _ptr_filename, Rect _posRes, Game* _ptr_game, int _layer);
	~StaticObj();

	void Update(float _deltaTime) override;
	void Render(Renderer* _ptr_renderer) override;
};