#include <iostream>
#include "Renderer.h"
#include "Texture.h"

Renderer::Renderer(SDL_Window* _ptr_window)
{
	m_ptr_renderer = SDL_CreateRenderer(_ptr_window, -1, SDL_RendererFlags::SDL_RENDERER_ACCELERATED | SDL_RendererFlags::SDL_RENDERER_PRESENTVSYNC);
	if (m_ptr_renderer == nullptr)
	{
		std::cout << "\nError while initializing renderer\n";
		return;
	}
}
SDL_Renderer* Renderer::GetRenderer()
{
	return m_ptr_renderer;
}

void Renderer::Clear()
{
	SDL_RenderClear(m_ptr_renderer);
}
void Renderer::Present()
{
	SDL_RenderPresent(m_ptr_renderer);
}
void Renderer::DrawTexture(Texture* _ptr_texture, Rect _destination)
{
	SDL_RenderCopy(m_ptr_renderer, _ptr_texture->GetTexture(), nullptr, &_destination);
}
void Renderer::DrawTexture(Texture* _ptr_texture, Rect _source, Rect _destination)
{
	SDL_RenderCopy(m_ptr_renderer, _ptr_texture->GetTexture(), &_source, &_destination);
}
void Renderer::DrawText(char* _ptr_text, Font* _ptr_font, SDL_Color _color, Vector2 _position)
{
	SDL_Surface* surface = TTF_RenderText_Blended(_ptr_font->m_Font, _ptr_text, _color);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(m_ptr_renderer, surface);
	int width;
	int height;

	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);

	SDL_Rect destination;
	destination.w = width;
	destination.h = height;
	destination.x = _position.m_X;
	destination.y = _position.m_Y;

	SDL_RenderCopy(m_ptr_renderer, texture, nullptr, &destination);

	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
}