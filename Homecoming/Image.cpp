#include "Image.h"

// By Viki

Image::Image(char* _ptr_Filename, Rect _posRect, Game* _ptr_Game, int _layer) : Entity(_ptr_Game, _layer)
{
	m_Position = Vector2(_posRect.x, _posRect.y);
	m_Resolution = Vector2(_posRect.w, _posRect.h);
	m_ptr_Texture = new Texture(_ptr_Filename, _ptr_Game->m_ptr_Renderer);
}
Image::~Image()
{
	delete m_ptr_Texture;
	m_ptr_Texture = nullptr;
}

void Image::Render(Renderer* _ptr_Renderer)
{
	_ptr_Renderer->DrawTexture(m_ptr_Texture, Rect(m_Position, m_Resolution));
}