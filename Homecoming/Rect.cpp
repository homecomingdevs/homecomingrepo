#include "Rect.h"

Rect::Rect()
{
	this->x = 0;
	this->y = 0;
	this->w = 0;
	this->h = 0;
}
Rect::Rect(Vector2 _position, Vector2 _resolution)
{
	this->x = _position.m_X;
	this->y = _position.m_Y;
	this->w = _resolution.m_X;
	this->h = _resolution.m_Y;
}