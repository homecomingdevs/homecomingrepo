#include "Font.h"
#include <iostream>

using namespace std;

Font::Font(char* _filename, int _size)
{
	m_Font = TTF_OpenFont(_filename, _size);

	if (m_Font == nullptr)
	{
		cout << _filename << " konnte nicht geladen werden: " << TTF_GetError() << endl;
	}
}
Font::~Font()
{
	TTF_CloseFont(m_Font);
}